# miniops

python package to efficiently consume dataframes stored in json files in minio

`pip install minio`

### Example

see the jupyter notebook [demo.ipynb](demo.ipynb)

### Aknowledgement

Miniops had been developed by TNO in the SOCCRATES innovation project. SOCCRATES has received funding from the European Union’s Horizon 2020 Research and Innovation program under Grant Agreement No. 833481.
